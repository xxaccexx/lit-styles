import { resolve, dirname } from '@acce/lit-path';

const pending = new Map();
const complete = new Map();
const failed = new Map();

const getPathname = path => {
	const a = document.createElement('a');
	a.href = path;
	return a.pathname;
}

const getStylesheet = async path => {
	const sheet = new CSSStyleSheet();
	const fullPath = `${location.protocol}//${location.host}${path}`;

	const response = await fetch(path);

	if (!response.ok)
		throw new Error(`GET ${fullPath} ${response.status} (${response.statusText})`);

	sheet.replace(await response.text());

	return sheet;
}

export const importStyle = async (stylePath, rootPath = location.href) => {
	const importPath = resolve(dirname(getPathname(rootPath)), stylePath);

	if (failed.has(importPath)) throw failed.get(importPath);
	if (complete.has(importPath)) return complete.get(importPath);
	if (pending.has(importPath)) return pending.get(importPath);

	const sheetProm = getStylesheet(importPath)
		.then(sheet => {
			complete.set(importPath, sheet);
			pending.delete(importPath);
			return sheet;
		})

		.catch(e => {
			pending.delete(importPath);
			failed.set(importPath, e);

			throw e;
		});

	pending.set(importPath, sheetProm);
	return await sheetProm;
}

export const adoptStyle = (sheet, parent) => {
	if (parent instanceof HTMLElement) {
		return adoptStyle(parent.shadowRoot, sheet);
	}

	if (!(parent instanceof Document) && !(parent instanceof ShadowRoot))
		throw new TypeError('parent must be a Document or ShadowRoot. Or an element that has a ShadowRoot');

	parent.adoptedStyleSheet = [...(parent.adoptedStyleSheet || []), sheet];
}
